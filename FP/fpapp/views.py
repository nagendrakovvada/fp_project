from django.shortcuts import render
from .models import *
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
from django.contrib.auth.models import User
import json
from django.contrib.sessions.models import Session

def index(request):
	return render(request, 'index3.html')
	# return render_to_response('index3.html', locals())

def logout(request):
	try:
		del request.session["username"]
	except KeyError:
		pass
	request.session.flush()
	return HttpResponseRedirect('/index/')

@csrf_exempt
def register(request):
	JSONer = {}
	if request.method == "POST":
		info = request.POST
		email = info['reg_email']
		password = info['password']
		firstname = info['firstname']
		lastname = info['lastname']
		username = info['username']
		mobile = info['mobile']
		user_id_valid = user_id_valid = User.objects.filter(username=username)
		JSONer={}
		if not user_id_valid:
			User.objects.create(username=username, email=email, password=password,first_name=firstname, last_name=lastname)
			JSONer['msg'] = "Hey " + info['firstname'] + ", Kudos. You are Successfully registered ! "
			JSONer["status_code"] = 200
		else:
			JSONer['msg'] = "User Already registered !!"
			JSONer['code'] = 400
	else:
		JSONer['msg'] = "Invalid Request"
		JSONer['code'] = 405
	json_data = json.dumps(JSONer)
	return JsonResponse(JSONer)

@csrf_exempt
def login(request):
	JSONer = {}
	if request.method == "POST":
		info = request.POST
		user_id_valid = User.objects.filter(username=info["username"],password=info["password"])
		if user_id_valid:
			JSONer['msg'] = 'Hello..!, Logging you in. Please wait.'
			JSONer['code'] = 200
			# Session.objects.all().delete()
			request.session["username"] = info["username"]
			json_data = json.dumps(JSONer)
			return JsonResponse(JSONer)
	else:
		JSONer['msg'] = "Please fill the valid login credentials."
		JSONer['code'] = 400
		json_data = json.dumps(JSONer)
	return JsonResponse(JSONer)

@csrf_exempt
def info(request):
	JSONer = {}
	if request.method == "POST":
		uploaded_file = request.FILES['file-upload']
		str_text = ''
		for line in uploaded_file:
			str_text = str_text + line.decode()
		json_data = json.loads(str_text)
		try:
			for val in json_data:
				User_data.objects.create(**val)
			JSONer['msg'] = "Data Added"
			JSONer['code'] = 200
		except:
			JSONer['msg'] = "Problem with adding New/Duplicate Data"
			JSONer['code'] = 400			
		return JsonResponse(JSONer)
	else:
		query_results = User_data.objects.all()
		count = query_results.count()
		return render(request, 'index2.html',locals())