from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class User_data(models.Model):
	userId		= models.IntegerField()
	title		= models.CharField(max_length=70)
	body		= models.CharField(max_length=70)
	def __str__(self):
		return self.userId,self.title