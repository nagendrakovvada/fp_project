function register(){
        var firstname = $("#fnamesignup").val();
        var lastname = $("#lnamesignup").val();
        var username = $("#usernamesignup").val();
        var reg_email = $("#emailsignup").val();
        var mobile = $("#mobilesignup").val();
        var password = $("#passwordsignup").val();
        var confirm_password = $("#passwordsignup_confirm").val();
       var body = {
            'firstname': firstname,
            'lastname': lastname,
            'reg_email': reg_email,
            'mobile': mobile,
            'password': password,
            'confirm_password': confirm_password,
            'username':username
            }
        console.log(body)
        $.ajax({
        type:'POST',
        url:'/register/',
        data:body,
        dataType: 'json',
        // complete: function(){
        //     $("#register_loader").attr('hidden', true);
        // },
        // beforeSend: function(){
        //     $("#register_loader").attr('hidden', false);
        // },
        success: function(JSONer) {
            console.log(JSONer);
            if(JSONer.code == 200){
                $("#register_success").html(JSONer.msg);
            }else{
                $("#register_success").html(JSONer.msg);
            }
        },
        error: function(response){
            console.log(response);
            $("#register_success").attr('hidden', false);
            $("#register_success").html("Something unexpected happened.");
            $("#register_success").slideUp(3000);
        }
        });
}



function login(){

    var username = $("#username").val();
    var password = $("#password").val();
    var data = {
        'username': username,
        'password': password
    }
    $.ajax({
    type: 'POST',
    url:'/login/',
    data: data,
    dataType: 'json',
    success: function(JSONer) {
        console.log(JSONer);
        if(JSONer.code == 200){
            $("#login_success").html(JSONer.msg);
            $(location).attr('href','/info/');
        }else{
            $("#login_success").html(JSONer.msg);
            setTimeout(function(){
                        $("#login_error").slideUp();
            }, 4000);
        }
    },
    error: function(result){
        console.log(result);
        $("#login_success").html("Something unexpected happened.");
        $("#login_success").slideUp(3000);
    }
    });

}


function upload(){
    var data = new FormData();
    var file_upload = $("#file_upload").prop('files');
    data.append('file-upload', file_upload[0],file_upload.name);
    $.ajax({
        url: "/info/",
        type: "POST",
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        // beforeSend: function(){
        //     $('.progress').attr('hidden',false);
        //     $('.progress-bar').css({'width':'20%'});
        //     setTimeout(function(){
        //     $('.progress-bar').css({'width':'40%'});
        //     }, 2000);
        //     setTimeout(function(){
        //     $('.progress-bar').css({'width':'80%'});
        //     }, 2200);
        // },
        // complete: function(response){

        // },
        success: function(JSONer){
             // $('tbody').append("")
            if(JSONer.code == 200){
             location.reload();
            }
            else{
                setTimeout(function(){
                $("#login_success").html(JSONer.msg);
            }, 2000);
        }
        },
        error: function(data){
            alert('error while creating an event.')
        }
    });

    }